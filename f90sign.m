function output=f90sign(a,b)
  if(b>=0)
      output=abs(a);
  else
      output=-abs(a);
  end
end