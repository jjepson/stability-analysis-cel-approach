function output=nc_poly(s,m,ar,br)
      p1=1.0;
      p2=0.0;
      for j=1:m
        p3=p2;
        p2=p1/br(j-1+1);
        p1=(s - ar(j-1+1))*p2-br(j-1+1)*p3;
      end
      output = p1;
end