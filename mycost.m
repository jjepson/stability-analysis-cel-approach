function output=mycost(M1lamf,M1othf,krange,dt,t0,xin)
%   thv1=xin(1);
%   thn=xin(2);
%   tht1=xin(3);
%   tht2=xin(4);
%   thte1=xin(5);
%   thte2=xin(6);
%   thb1=xin(7);
%   thb2=xin(8);
%   thft=xin(9);
%   thfn=xin(10);
%   thfb=xin(11);
%   thfu=xin(12);
%   ft_simul=xin(13);
%   fu_simul=xin(14);
%   thf1=xin(15);
%   thf2=xin(16);
%   thf3=xin(17);
%   thfc1=xin(18);
%   thfc2=xin(19);
  xint=num2cell(xin);
  lambda=cell(1,numel(krange));
  omegmax=0.0;
  for i=1:numel(krange)
     omegtmp=max((imag(-log(eig(-inv(M1lamf(krange(i),xint{:}))*M1othf(krange(i),xint{:})))/1i/(dt/t0))));
     lambda{i}=(-log(eig(-inv(M1lamf(krange(i),xint{:}))*M1othf(krange(i),xint{:})))/1i/(dt/t0));
     if(omegtmp>omegmax)
         omegmax=omegtmp;
     end
  end
  output=omegmax;
%   figure(5)
%   scatter(krange,real(cell2mat(lambda)));
  figure(6)
  scatter(krange,imag(cell2mat(lambda)));
end