function x_node=poly_nodes(nl,np,nt,gll)
  nq=(2*np+nt)*nl+1;  
  x_node=zeros(1,nq);

  if (gll) 
    for j=1:2*np+nt
       [x_node((j-1)*nl+1:j*nl+1),~]=lobleg(-1,1,nl+1);
       x_node((j-1)*nl+1:j*nl+1)=x_node((j-1)*nl+1:j*nl+1)*0.5+0.5+(j-1);
    end
  else
    for j=1:2*np+nt
      for i=(j-1)*nl+1:j*nl+1
         x_node(i)=(i-1)/(nl);
      end
    end
  end
end