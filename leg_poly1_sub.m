%-----------------------------------------------------------------------
%     subprogram 13. leg_poly1_sub.
%
%     This subroutine evaluates a single Legendre polynomial and its
%     derivative, using recurrence.  The coding for the value is
%     extracted from gauleg.
%
%     The argument list is:
%
%     xx [real] {input} -- The value of the independent variable in
%        the standard -1 <= xx <= +1 range.
%     nn [integer] {input} -- The index of the polynomial.
%     val [real] {output} -- The value of L_n(xx).
%     drv [real] {output} -- The derivative of L_n(xx).
%-----------------------------------------------------------------------
function [val,drv]=leg_poly1_sub(xx,nn)
      val=1;
      drv=0;
      p2=0;
      for j=1:nn
        p3=p2;
        p2=val;
        val=((2*j-1)*xx*p2-(j-1)*p3)/(j);
        drv=xx*drv+(j)*p2;
      end
end