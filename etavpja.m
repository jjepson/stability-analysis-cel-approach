function [vp,ja,ptch]=etavpja(nl,np,nt,eta)
    %th_tp = acos(tpb);
    syms eta1;
    th_tp=pi/3;
    %i = 0; 
    nq=(2*np+nt)*nl+1;
    tht=pi-2.*th_tp;
    m_xi_pp =np; % # of cells in + passing space

    vp1= piecewise(eta1<=np,-cos(th_tp*eta1/np),eta1>np & eta1 <= np+nt,-cos(th_tp + tht*(eta1-np)/nt),...
        -cos(th_tp + tht + th_tp*(eta1-np-nt)/m_xi_pp));
    ja1= piecewise(eta1<=np,th_tp/np*sin(th_tp*eta1/np),eta1>np & eta1 <= np+nt,tht/nt*sin(th_tp + tht*(eta1-np)/nt),...
        th_tp/m_xi_pp*sin(th_tp + tht + th_tp*(eta1-np-nt)/m_xi_pp));
    ptch1= piecewise(eta1<=np,sin(th_tp*eta1/np)/(th_tp/np),eta1>np & eta1 <= np+nt,sin(th_tp + tht*(eta1-np)/nt)/(tht/nt),...
        sin(th_tp + tht + th_tp*(eta1-np-nt)/m_xi_pp)/(th_tp/m_xi_pp));

    
    if(isnumeric(eta))
      vp=double(subs(vp1,eta1,eta));
      ja=double(subs(ja1,eta1,eta));
      ptch=double(subs(ptch1,eta1,eta));
    else
      vp=subs(vp1,eta1,eta);
      ja=subs(ja1,eta1,eta);
      ptch=subs(ptch1,eta1,eta);
    end

%     %negative passing space
%     for iy=0:np-1 
%         i = i+1;
%         th=th_tp*eta/m_xip;
%         vp(i) = -cos(th);
%         ja(i) = th_tp*sin(th)/m_xip;
%     end
% 
%     %trapped space
%     tht=pi-2.*th_tp;
%     for iy=m_xip:m_xip+m_xit-1 
%       i = i+1;
%       for ig=1:nq_xi
%         th=th_tp + tht*(eta-m_xip)/m_xit;
%         vp(i,ig) = -cos(th);
%         ja(i,ig) = tht*sin(th)/m_xit;
%       end
%     end
% 
%     %positive passing space
%     m_xi_pp = m_xi - m_xip - m_xit; % # of cells in + passing space
%     for iy=m_xip+m_xit:m_xi-1 
%       i = i+1;
%       for ig=1:nq_xi
%         th=th_tp + tht + th_tp*(eta-m_xip-m_xit)/m_xi_pp;
%         vp(i,ig) = -cos(th);
%         ja(i,ig) = th_tp*sin(th)/m_xi_pp;
%       end
%     end
end