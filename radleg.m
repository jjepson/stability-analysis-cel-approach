%-----------------------------------------------------------------------
%     subprogram 8. radleg.
%     abscissas and weights for Radau-Legendre integration, based on
%     Abromowitz and Stegun.
%-----------------------------------------------------------------------
function [x,w]=radleg(x1,x2,n)
      myeps=1.e-15;
      xgau=zeros(1,n-1);
      wgau=zeros(1,n-1);
      xgaun=zeros(1,n);
      wgaun=zeros(1,n);
      x=zeros(1,n);
      w=zeros(1,n);

%-----------------------------------------------------------------------
%     the zeros of the Legendre polynomials of degree n-1 and n are used
%     to start Newton's method for finding the zeros of their sums.
%-----------------------------------------------------------------------
      nleg=n-1;
      [xgau,wgau]=gauleg(-1,1,nleg);
      [xgaun,wgaun]=gauleg(-1,1,n);
%-----------------------------------------------------------------------
%     the GRL node locations are the zeros of the polynomial
%
%      (L_nleg+L_(nleg+1))/(x+1) = L_nleg+(x-1)*[d(L_nleg)/dx]/(nleg+1)
%
%     the interval -1<=x<=1.  Use 1st-order Taylor expansion about zeros
%     of the two Legendre polynomials on the lhs for an initial guess.
%     to start each Newton iteration.  Use recurrence to
%     evaluate
%         g(x)=L_nleg+(x-1)*[d(L_nleg)/dx]/(nleg+1)
%     and
%         dg/dx={nleg*L_nleg+
%                [(1+x)*(nleg+1)+1-x]*[d(L_nleg)/dx]/(nleg+1)}/(1+x)
%-----------------------------------------------------------------------
      m=(n+1)/2;
      xm=0.5*(x2+x1);
      xl=0.5*(x2-x1);
      fac=xl/(n^2);

      w(1)=2*fac;
      x(1)=x1;
      for i=2:n
        [gg,dg]=leg_poly1_sub(xgau(i-1),nleg);
        [ff,df]=leg_poly1_sub(xgaun(i),n);
        z=(xgau(i-1)*dg+xgaun(i)*df)/(dg+df);
        while(true)
          [ff,df]=leg_poly1_sub(z,nleg);
          gg=ff+(z-1)*df/(n);
          dg=((n)*ff+(z+1)*df-gg)/(z+1);
          z1=z;
          z=z1-gg/dg;
          if (abs(z-z1)<=eps) 
              break
          end
        end
        w(i)=fac*(1-z)/ff^2;
        x(i)=xm+xl*z;
      end

end