function output=etabasis(nl,np,nt,eta,gll,dmode)
      x_node=zeros(1,nl+1);
      nq=(2*np+nt)*nl+1;
      if(isnumeric(eta))
        output=zeros(1,nq);
      else
        output=vpa(zeros(1,nq));
      end

      if (gll) 
        [x_node,~]=lobleg(-1,1,nl+1);
        x_node=x_node*0.5+0.5;
      else
        for i=1:nl+1
            x_node(i)=(i-1)/(nl);
        end
      end
      c_norm=ones(1,nl+1);
      for i=1:nl+1
        for j=1:nl+1
          if (j==i) 
              continue
          end
          c_norm(i)=c_norm(i)/(x_node(i)-x_node(j));
        end
      end
      if (dmode~=1)
          for k=1:2*np+nt
              if(isnumeric(eta))
                al=ones(1,nl+1);
              else
                al=vpa(ones(1,nl+1));
              end
              for i=1:nl+1
                al(i)=c_norm(i);
                for j=1:nl+1
                  if (j==i) 
                      continue
                  end
                  al(i)=al(i)*((eta-k+1)-x_node(j));
                end
              end
              if(isnumeric(eta))
                if(k==2*np+nt)
                  output(1+(k-1)*(nl):k*nl+1)=output(1+(k-1)*(nl):k*nl+1)+al*(eta<=k)*(eta>=(k-1));
                else
                  output(1+(k-1)*(nl):k*nl+1)=output(1+(k-1)*(nl):k*nl+1)+al*(eta<k)*(eta>=(k-1));
                end
              else
                if(k==2*np+nt)
                  output(1+(k-1)*(nl):k*nl+1)=output(1+(k-1)*(nl):k*nl+1)+al*piecewise(eta<=k & eta>=(k-1),1,0);
                else
                  output(1+(k-1)*(nl):k*nl+1)=output(1+(k-1)*(nl):k*nl+1)+al*piecewise(eta<k & eta>=(k-1),1,0);
                end
              end
          end
      else
          for k=1:2*np+nt
              if(isnumeric(eta))
                dal=zeros(1,nl+1);
              else
                dal=vpa(zeros(1,nl+1));
              end
              for i=1:nl+1
                  for kk=1:nl+1
                      if (kk==i) 
                          continue
                      end
                      dxtmp=c_norm(i);
                      for j=1:nl+1
                        if (j==i) 
                            continue
                        end
                        if (j==kk) 
                            continue
                        end
                        dxtmp=dxtmp*((eta-k+1)-x_node(j));
                      end
                      dal(i)=dal(i)+dxtmp;
                  end
              end
              if(isnumeric(eta))
                if(k==2*np+nt)
                  output(1+(k-1)*(nl):k*nl+1)=output(1+(k-1)*(nl):k*nl+1)+dal*(eta<=k)*(eta>=(k-1));
                else
                  output(1+(k-1)*(nl):k*nl+1)=output(1+(k-1)*(nl):k*nl+1)+dal*(eta<k)*(eta>=(k-1));
                end
              else
                if(k==2*np+nt)
                  output(1+(k-1)*(nl):k*nl+1)=output(1+(k-1)*(nl):k*nl+1)+dal*piecewise(eta<=k & eta>=(k-1),1,0);
                else
                  output(1+(k-1)*(nl):k*nl+1)=output(1+(k-1)*(nl):k*nl+1)+dal*piecewise(eta<k & eta>=(k-1),1,0);
                end
              end
          end
      end
end