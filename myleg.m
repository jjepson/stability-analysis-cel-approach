function [output]=myleg(n,x,diff)

    if ~exist('diff','var')
     % third parameter does not exist, so default it to something
      diff = false;
    end
    nmax=max(n);
    nmin=min(n);
    if(numel(n)>1)
      output1=zeros(1,nmax+1);
      output2=zeros(size(n));
      output=zeros(size(n));
    else
      output1=squeeze(zeros([n+1 size(x)]));
      if(n==0)
          output1=output1.';
      end
      output2=zeros(size(x));
      output=zeros(size(x));
    end
    p1=ones(size(x));
    p2=zeros(size(x));
    ctr=1;
%     if(nmin==0)
      if(numel(n)>1)
        output1(ctr)=p1;
      else
        output1(ctr,:)=p1;
      end
      ctr=ctr+1;
%     end
    for j=1:nmax
       p3=p2;
       p2=p1;
       p1=((2*j-1)*x.*p2-(j-1)*p3)/(j);
%        if(j >= nmin)
         if(numel(n)>1)
           output1(ctr)=p1;
         else
           output1(ctr,:)=p1;
         end
         ctr=ctr+1;
%        end
    end
    if(~diff)
        if(numel(n)>1)
          output=output1(nmin+1:nmax+1);
        else
          output=output1(n+1,:);
        end
        return;
    end
    p1=ones(size(x));
    p2=zeros(size(x));
    ctr2=1;
    if(nmin==0)
      if(numel(n)>1)
        output2(ctr2)=p2;
      else
        output2=p2;
      end
      ctr2=ctr2+1;
      if(nmax>=1)
        if(numel(n)>1)
            output2(ctr2)=p1;
        else
            output2=p1;
        end
        ctr2=ctr2+1;
      end
    elseif(nmin==1)
      if(numel(n)>1)
          output2(ctr2)=p1;
      else
          output2=p1;
      end
      ctr2=ctr2+1;
    end
    for j=2:nmax
       p3=p2;
       p2=p1;
       if(numel(n)>1)
         p1=((2*j-1)*x.*p2-(j-1)*p3)/(j)+(2*j-1)/j*output1(j);
       else
         p1=((2*j-1)*x.*p2-(j-1)*p3)/(j)+(2*j-1)/j*output1(j,:);
       end
       if(j >= nmin)
         if(numel(n)>1)
             output2(ctr2)=p1;
         else
             output2=p1;
         end
         ctr2=ctr2+1;
       end
    end
    output=output2;
end