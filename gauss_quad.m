function [x,w,ar,br]=gauss_quad(a,b,ns,N)
      x=zeros(1,ns);
      w=zeros(1,ns); 
      ar=zeros(1,ns+1);
      br=zeros(1,ns+1);
      x1=zeros(1,N);
      w1=zeros(1,N); 
      ar0=zeros(1,ns+1);
      br0=zeros(1,ns+1);
      f=zeros(1,3);

      hg=1.e15;
%-----------------------------------------------------------------------
%     Gauss-Legendre quadrature used to determine ns recursion          
%     coefficients ar0(0:ns) and br0(0:ns).                             
%-----------------------------------------------------------------------
      [x1,w1]=gauleg(-1,1,N);
%-----------------------------------------------------------------------
%     handle mapping for different domains.                             
%-----------------------------------------------------------------------
      for i=1:N 
        if (a>-hg && b<hg) % 'fininteab'
           phi = 0.5*(x1(i)+1.0)*(b-a) + a; 
          dphi = 0.5*(b-a); 
        elseif (a>-hg && b>hg) % 'semi-inf'
           phi = (2.0*a+1.0+x1(i))/(1.0-x1(i)); 
          dphi = 2.0*(1.0+a)/(1.0-x1(i))^2; 
        elseif (a<-hg && b>hg) % 'infinite'
           phi = x1(i)/(1.0-x1(i)^2); 
          dphi = (1.0+x1(i)^2)/(1.0-x1(i)^2)^2; 
        end 
        x1(i) = phi;
        f(1)=exp(-phi^2);
        w1(i) = w1(i)*dphi*f(1);
      end
%-----------------------------------------------------------------------
%     here pm  refers to unnormalized polynomial of degree m            
%     and  pbm refers to   normalized polynomial of degree m.           
%-----------------------------------------------------------------------
                                        % SQRT(int(w*p0**2))            
      br0(0+1) = sqrt(sum(w1)); 
                                        % int(w*x*pb0**2)               
      ar0(0+1) = sum(w1(1:N).*x1(1:N))/br0(0+1)^2;
      for m=1:ns 
        for i=1:N 
          if (w1(i)==0) 
              break
          end
          pm = nc_poly(x1(i),m,ar0(0+1:m-1+1),br0(0+1:m-1+1)); 
          br0(m+1) = br0(m+1) + w1(i)*pm^2; 
        end 
        br0(m+1) = sqrt(br0(m+1)); 
        for i=1:N 
          if (w1(i)==0) 
              break
          end
          pm = nc_poly(x1(i),m,ar0(0+1:m-1+1),br0(0+1:m-1+1)); 
          ar0(m+1) = ar0(m+1) + w1(i)*x1(i)*(pm/br0(m+1))^2; 
        end 
      end 
%-----------------------------------------------------------------------
%     Include preassigned nodes if desired.                             
%     implement Gauss-Radau (extra node at a) or                        
%               Gauss-Lobatto (extra node at a and b) rules.            
%-----------------------------------------------------------------------
%       IF (PRESENT(np)) THEN
%         IF (np /= 0) THEN 
%           ALLOCATE(ld(1:ns),ud(1:ns),dd(1:ns),r(1:ns),sol(1:ns))
%           DO ip=1,np 
% !           Set up parts of linear system that are the same.                
%             IF (ip==1) THEN 
%               ld = 0._r8 ; ud = 0._r8 ; dd = 0._r8 
%               DO i=1,ns-1 
%                 ld(i+1) = br0(i) 
%                 ud(i) = br0(i) 
%               ENDDO 
%             ENDIF 
%             r = 0._r8 
%             IF (np==2) THEN 
%               r(ns-1) = 1._r8 
%             ELSE 
%               r(ns-1) = br0(ns-1)**2 
%             ENDIF 
% !           Diagonal depends on location of pre-assigned node.              
%             DO i=1,ns-1 
%               dd(i) = ar0(i-1) - xp(ip) 
%             ENDDO 
% !           Solve system tridiagonal system.                                
%             CALL tridiag(ld,dd,ud,r,sol,ns-1_i4) 
% !           Store results for eigen computation of weights and nodes.       
%             IF (ip==1) THEN 
%               gamma_n = sol(ns-1) 
%             ELSE 
%               mu_n = sol(ns-1) 
%             ENDIF 
%           ENDDO 
% !         Form symmetric, "Jacobian" matrix.                                
%           IF (np==1) THEN 
%             ar0(ns-1) = xp(1)+gamma_n 
%           ELSE
%             det = gamma_n-mu_n 
%             ar0(ns-1) = (xp(2)*gamma_n-xp(1)*mu_n)/det 
%             br0(ns-1) = SQRT((xp(2)-xp(1))/det) 
%           ENDIF
%         ELSE
% !          WRITE(*,*) 'no pre-assigned nodes in quadrature scheme' 
%         ENDIF
%       ELSE 
% !        WRITE(*,*) 'no pre-assigned nodes in quadrature scheme' 
%       ENDIF 
%-----------------------------------------------------------------------
%     solve eigensystem for nodes, x, and weights, w.                   
%     ar0 and br0 get overwritten in gaucof so store them if desired.   
%-----------------------------------------------------------------------
      ar(0+1:ns+1) = ar0(0+1:ns+1);  % store recursion coefficients
      br(0+1:ns+1) = br0(0+1:ns+1);  % store recursion coefficients
      amu0 = br0(0+1)^2; 
%       SM=diag((ar(1:ns).'))+diag(((br(2:ns).')),1)+diag(((br(2:ns).')),-1);
%       %[T,B]=balance(SM);
%       [V,D]=eig(SM,'nobalance');
%       %disp(V)
%       %V=T*V;
% %        for i=1:ns
% %           V(:,i)=V(:,i)/sqrt(V(:,i).'*V(:,i));
% %        end
%       [x,In]=sort(diag(D).');
%       npi=double(pi);
% 
%        disp(V(1,In))
%        disp(amu0)
%       %w=sqrt(npi)/2*(V(1,:)).^2;
%       w=amu0*(V(1,:)).^2;
%       disp(sum(x.^159.*w(In)))
%       w=w(In)./exp(-x.^2);
      [ar0,br0,x,w]=gaucof(ns,ar0(0+1:ns-1+1),br0(0+1:ns-1+1),amu0,x,w) ;
%-----------------------------------------------------------------------
%     rearrange nodes in increasing order and incorporate weight        
%     function into weights so integrands appear in their natural form. 
%-----------------------------------------------------------------------
      x(1:ns) = x(ns:-1:1); 
      w(1:ns) = w(ns:-1:1); 
      for i=1:ns 
% This was dangerous as it led to poor quadrature in cases where 
% the weights came out less than 1.e-10.  This includes the
% w(s) = s**sps*exp(-s**2).  With ns>12 the final weights
% were less than 1.e-10
        %CALL f0_calc(x(i),sps,s_quads,f(0:2))
        f(1)=exp(-x(i)^2);
        if (f(1)==0) 
          disp(strcat('zero weight at',num2str(x(i))))
          %STOP
        end
        w(i) = w(i)/f(1);
      end
end
