close all;
clearvars;
format longE;


npi=double(pi);
%create speed polynomials
nsp=8;
nl=9;
np=1; 
nt=1; 
nq=(2*np+nt)*nl+1;
nc=2*np+nt;
cquad=200;
ncolleg=2*cquad-nl-5+1; %accuracy seems to be ~ 10^-14
%nvbar12=ceil((nsp+ncolleg+5)/2);
nvbar12=cquad;
%nvbar3=ceil((nsp+3)/2);
nvbar3=nvbar12;
%nvbar3=128;

bigN=1600;
[sq,wq,ar,br]=gauss_quad(0,1e16,nsp,bigN);

ns2_2=(nsp+2)/2;
vfac=100;
for i=1:50
    vfac=sqrt(ns2_2*(1+2*log(vfac)-log(ns2_2))-log(eps));
end
vbarmax=vfac./sq;
do_domain_3=false(1,nsp);
for i=1:nsp
  if(vbarmax(i)>2)
     do_domain_3(i)=true;
  end
end

[vbar1q,wbar1q]=gauleg(0,1,nvbar12);
%  vbar1q=1-vbar1q;
%  vbar1q=vbar1q(nvbar12:-1:1);
%  wbar1q=wbar1q(nvbar12:-1:1);
[vbar2q,wbar2q]=gauleg(1,2,nvbar12);
vbar3q=zeros(nsp,nvbar3);
wbar3q=zeros(nsp,nvbar3);
for i=1:nsp
  if(do_domain_3(i))
    [vbar3q(i,:),wbar3q(i,:)]=gauleg(2,vbarmax(i),nvbar3);
  end
end

% bigN=1600;
% [vbar1q,wbar1q,~,~]=gauss_quad_vbarabs(0,1,nvbar12,bigN);
% [vbar2q,wbar2q,~,~]=gauss_quad_vbarabs(1,2,nvbar12,bigN);


[Lmat,Lpmat0,Lppmat0]=nc_poly_arrs(sq,nsp,ar(1:nsp),br(1:nsp));
Lpmat=(Lpmat0.'.*exp(-sq.^2)+Lmat.'.*(-2.*sq.*exp(-sq.^2))).';
Lppmat=(Lppmat0.'.*exp(-sq.^2)+2*Lpmat0.'.*(-2*sq.*exp(-sq.^2))+Lmat.'.*(4*sq.^2-2.0).*exp(-sq.^2)).';
Lvbarmat=zeros(nsp,nsp,2*nvbar12+nvbar3);
for j=1:nvbar12
   Lmatvb1=nc_poly_arrs(sq*vbar1q(j),nsp,ar(1:nsp),br(1:nsp));
   Lmatvb2=nc_poly_arrs(sq*vbar2q(j),nsp,ar(1:nsp),br(1:nsp));
   Lvbarmat(:,:,j)=(wq.'.*Lmat)*((Lmatvb1).'.*exp(-(sq.*vbar1q(j)).^2)).*wbar1q(j);
   Lvbarmat(:,:,j+nvbar12)=(wq.'.*Lmat)*((Lmatvb2).'.*exp(-(sq.*vbar2q(j)).^2)).*wbar2q(j);
end
for j=1:nvbar3
   Lmatvb3=nc_poly_arrs(sq.*((vbar3q(:,j)).'),nsp,ar(1:nsp),br(1:nsp));
   Lvbarmat(:,:,j+2*nvbar12)=(wq.'.*Lmat)*((Lmatvb3).'.*exp(-(sq.*(vbar3q(:,j)).').^2)).*(wbar3q(:,j).');
end

Lmat(abs(Lmat)<10^-14)=0;
Lpmat(abs(Lpmat)<10^-14)=0;
Lppmat(abs(Lppmat)<10^-14)=0;

gll=true;
dmode=0;
P= @(eta1) etabasis(nl,np,nt,eta1,gll,dmode);
dmode=1;
Pdiff=@(eta1) etabasis(nl,np,nt,eta1,gll,dmode);
% diffP=diff(P(etasp));
% diffPf=@(xg) subs(diffP,etasp,xg);
[xiarr,~,~]=etavpja(nl,np,nt,poly_nodes(nl,np,nt,gll));
%[xi,ja,ptch]=etavpja(nl,np,nt,etasp);


[xq_c,wq_c]=lobleg(0,1,cquad);
Pcoup=zeros(ncolleg,nq);
xiqarr=zeros(1,cquad*nc);
jaqarr=zeros(1,cquad*nc);
ptchqarr=zeros(1,cquad*nc);
for i=1:nc
  [xiqarr((i-1)*cquad+1:i*cquad),jaqarr((i-1)*cquad+1:i*cquad),ptchqarr((i-1)*cquad+1:i*cquad)]=etavpja(nl,np,nt,xq_c+(i-1));
end


for i=1:nc
  for j=1:cquad
      Pcoup=Pcoup+wq_c(j)*myleg(0:ncolleg-1,xiqarr(j+(i-1)*cquad)).'*jaqarr(j+(i-1)*cquad)*P(xq_c(j)+(i-1));
      %myint2=myint2+2*wtest(i)*legendreP(0:ncolleg-1,2*(xtest(i)-0.5)).'*legendreP(0:ncolleg-1,2*(xtest(i)-0.5));
  end
end

%etavpjaf=@(eta1) etavpja(nl,np,nt,eta1);

fpmat=(wq.'.*Lmat*(Lpmat.'));
fppmat=(wq.'.*Lmat*(Lppmat.'));

%ang=npi/2.0;
ang=0.0;
udk=1000.0;
%udk=0.0;
dt=1.0*10^-7;
mi=3.343*10^-27;kb=1.602*10^-19;t=3.0*10^3;n=0.948676*10^20;zeff=3.0;eps0=8.85*10^-12;ee=1.602*10^-19;b0=2.0;cs=cos(ang);sn=sin(ang);mu0=4.0*npi*10^-7;kpare=0*n*1.0*10^8;gam=5.0/3.0;te=4.6136*10^3;
me=9.109*10^-31;
eta=8.99*10.0^(-3)*mu0;
%eta=0.0;
wp=(n/zeff*ee^2*zeff^2/eps0/mi)^0.5;
t0=1.0/(zeff*ee*b0/mi); l0=1.0/(eps0*mu0)^0.5/(n/zeff*ee^2*zeff^2/eps0/mi)^0.5; v0=b0/(mu0*mi*n/zeff)^0.5;
vt=(2.0*kb*t/mi)^(1.0/2);
fm1= n/zeff/(vt^3.0*npi^(1.5));

f0=double(zeros(nq,nsp));
%f0=fm1*0.1*ones(nq,1)*exp(-sq.^2);
%f0=fm1*0.1*xiarr.'*(sq.*exp(-sq.^2));
%f0=fm1*0.1*xiarr.'*(sq.*exp(-sq.^2))*10^-10;

fbar0=(f0.*wq)*Lmat;
f0p=fbar0*(Lpmat.');
f0pp=fbar0*(Lppmat.');
f0(abs(f0)<10^-14)=0;
fbar0(abs(fbar0)<10^-14)=0;
f0p(abs(f0p)<10^-14)=0;
f0pp(abs(f0pp)<10^-14)=0;

pipar= @(lam,k,fk) (2.0*npi*mi*(2.0*kb*t/mi)^(5.0/2)*sum((Pcoup(2+1,:)*fk).*sq.^4.*wq));
pipardel= @(lam,k,fk) (2.0*npi*mi*(2.0*kb*t/mi)^(5.0/2)*sum((lam-1.0)*(Pcoup(2+1,:)*fk).*sq.^4.*wq));
pipar0=(2.0*npi*mi*(2.0*kb*t/mi)^(5.0/2)*sum((Pcoup(2+1,:)*f0).*sq.^4.*wq));
qpar= @(lam,k,fk) (npi*mi*(2.0*kb*t/mi)^(6.0/2)*sum((Pcoup(1+1,:)*fk).*sq.^5.*wq));
qpardel= @(lam,k,fk) (npi*mi*(2.0*kb*t/mi)^(6.0/2)*sum((lam-1.0)*(Pcoup(1+1,:)*fk).*sq.^5.*wq));
qpar0=(npi*mi*(2.0*kb*t/mi)^(6.0/2)*sum((Pcoup(1+1,:)*f0).*sq.^5.*wq));
% qpar0(abs(qpar0)<10^-7)=0;
% pipar0(abs(pipar0)<10^-7)=0;

momn= @(lam,k,fk) (2.0*npi*(2.0*kb*t/mi)^(3.0/2)*sum((Pcoup(0+1,:)*fk).*sq.^2.*wq));
momndel= @(lam,k,fk) (2.0*npi*(2.0*kb*t/mi)^(3.0/2)*sum((lam-1.0)*(Pcoup(0+1,:)*fk).*sq.^2.*wq));
momu= @(lam,k,fk) (2.0*npi*mi*(2.0*kb*t/mi)^(4.0/2)*sum((Pcoup(1+1,:)*fk).*sq.^3.*wq));
momudel= @(lam,k,fk) (2.0*npi*mi*(2.0*kb*t/mi)^(4.0/2)*sum((lam-1.0)*(Pcoup(1+1,:)*fk).*sq.^3.*wq));
momt= @(lam,k,fk) (npi*mi*(2.0*kb*t/mi)^(5.0/2)*sum((Pcoup(0+1,:)*fk).*sq.^4.*wq));
momtdel= @(lam,k,fk) (npi*mi*(2.0*kb*t/mi)^(5.0/2)*sum((lam-1.0)*(Pcoup(0+1,:)*fk).*sq.^4.*wq));


logc=23.0-log(zeff^2/t)-1.0/2*log(2.0*n*zeff*(10.0)^(-6)/t);
logcei=24-log(sqrt(n*(10.0)^(-6))/te);
nuii=double(ee^4*zeff^3*n*logc/(4.0*npi*eps0^2*mi^2*vt^3));
spitz=4*sqrt(2*npi)/3*zeff*kb^2*me^0.5*logcei/(4*npi*eps0)^2/(kb*te)^1.5/mu0;
fz=(1+1.198*zeff+.222*zeff^2)/(1+2.966*zeff+.753*zeff^2);
%nuii=0.0;
chi= @(s) (erf(s)-s.*2./(npi)^(1/2).*exp(-s.^2))./(2.*s.^2);

do_delt=0.0; c0=1.2/4;
%c0=1.14252/4;
%c0=1.0/4;

vxlhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (mi*n/zeff*((lam-1.0)*(v0*ukx)+thv1*dt*udk*1i*(k/l0)*(lam-1.0)*(v0*ukx))-dt^2*c0*(-(k/l0)^2)*(b0^2/mu0*(cs^2*(lam-1.0)*(v0*ukx))))/(mi*n/zeff*v0);
vxrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (-mi*n/zeff*dt*udk*1i*(k/l0)*(v0*ukx)+dt/mu0*1i*(k/l0)*((b0*bkx)*b0*cs)+dt*pipar0/b0*1i*(k/l0)*cs*(-(b0*bkx)))/(mi*n/zeff*v0);
vylhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (mi*n/zeff*((lam-1.0)*(v0*uky)+thv1*dt*udk*1i*(k/l0)*(lam-1.0)*(v0*uky))-dt^2*c0*(-(k/l0)^2)*(b0^2/mu0*(-cs*sn*(lam-1.0)*(v0*ukz)+ ...
    sn^2*(lam-1.0)*(v0*uky))+kb*gam*(n/zeff*t+n*te)*(lam-1.0)*(v0*uky)))/(mi*n/zeff*v0);
vyrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (-mi*n/zeff*dt*udk*1i*(k/l0)*(v0*uky)+dt/mu0*1i*(k/l0)*((b0*bkz)*b0*(-sn))-dt*1i*(k/l0)*kb*(n/zeff*(t*tk)+t*(n/zeff*nk)/zeff+n*(t*tek)+te*(n/zeff*nk))+ ... 
    dt*1.0/3*1i*(k/l0)*pipar(lam,(k/l0),(fu_simul*(lam-1.0)+1.0)*(fm1*fk))-dt*cs^2*1i*(k/l0)*pipar(lam,(k/l0),(fu_simul*(lam-1.0)+1.0)*(fm1*fk))+dt*2.5/3*pipar0/t*1i*(k/l0)*(t*tk)+dt*2.5*pipar0/t*(-cs^2*1i*(k/l0))*(t*tk)+dt*pipar0/b0*1i*(k/l0)*cs*(sn*(cs*(b0*bkz)-sn*(b0*bky)))+...
    dt*pipar0/b0*1i*(k/l0)*cs*(cs*(cs*(b0*bky)+sn*(b0*bkz))))/(mi*n/zeff*v0);
vzlhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (mi*n/zeff*((lam-1.0)*(v0*ukz)+thv1*dt*udk*1i*(k/l0)*(lam-1.0)*(v0*ukz))-dt^2*c0*(-(k/l0)^2)*(b0^2/mu0*(cs^2*(lam-1.0)*(v0*ukz)- ...
    cs*sn*(lam-1.0)*(v0*uky))))/(mi*n/zeff*v0);
vzrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (-mi*n/zeff*dt*udk*1i*(k/l0)*(v0*ukz)+dt/mu0*1i*(k/l0)*((b0*bkz)*b0*cs)-dt*cs*sn*1i*(k/l0)*pipar(lam,(k/l0),(fu_simul*(lam-1.0)+1.0)*(fm1*fk))+dt*2.5*pipar0/t*(-cs*sn*1i*(k/l0))*(t*tk)+...
    dt*pipar0/b0*1i*(k/l0)*cs*(-cs*(cs*(b0*bkz)-sn*(b0*bky)))+dt*pipar0/b0*1i*(k/l0)*cs*(sn*(cs*(b0*bky)+sn*(b0*bkz))))/(mi*n/zeff*v0);

nlhs= @(lam,k,ukx,uky,ukz,nk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) ((lam-1.0)*(n/zeff*nk)+thn*dt*udk*1i*(k/l0)*(lam-1.0)*(n/zeff*nk))/(n/zeff);
nrhs= @(lam,k,ukx,uky,ukz,nk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (-dt*udk*1i*(k/l0)*(n/zeff*nk)-dt*n*1i*(k/l0)*lam*(v0*uky))/(n/zeff);

tlhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (1.5*n*kb/zeff*((lam-1.0)*(t*tk)+tht1*dt*udk*1i*(k/l0)*(lam-1.0)*(t*tk))+tht2*dt*3*qpar0/t*1i*(k/l0)*cs*(lam-1.0)*(t*tk))/(1.5*n*kb/zeff*t);
trhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (-1.5*n*kb/zeff*dt*udk*1i*(k/l0)*(t*tk)-dt*n*t*kb/zeff*1i*(k/l0)*lam*(v0*uky)-dt*cs*1i*(k/l0)*qpar(lam,(k/l0),(ft_simul*(lam-1.0)+1.0)*(fm1*fk))+dt*qpar0/b0*1i*(k/l0)*cs*(cs*(b0*bky)+sn*(b0*bkz))+ ...
    dt*(-3.0)*qpar0/t*1i*(k/l0)*cs*(t*tk))/(1.5*n*kb/zeff*t);

telhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (1.5*n*kb*((lam-1.0)*(t*tek)+thte1*dt*udk*1i*(k/l0)*(lam-1.0)*(t*tek))+thte2*dt*kpare*cs^2*(k/l0)^2*kb*(lam-1.0)*(t*tek))/(1.5*n*kb*t);
terhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (-1.5*n*kb*dt*udk*1i*(k/l0)*(t*tek)-dt*n*te*kb*1i*(k/l0)*lam*(v0*uky)-dt*kpare*cs^2*(k/l0)^2*kb*(t*tek))/(1.5*n*kb*t);

bxlhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) ((lam-1.0)*(b0*bkx)+thb1*dt*udk*1i*(k/l0)*(lam-1.0)*(b0*bkx)+thb2*dt*eta/mu0*(k/l0)^2*(lam-1.0)*(b0*bkx))/b0;
bxrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (dt*(b0*cs*1i*(k/l0)*lam*(v0*ukx)-udk*1i*(k/l0)*(b0*bkx)-eta/mu0*(k/l0)^2*(b0*bkx)))/b0;
bylhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) ((lam-1.0)*(b0*bky)+thb1*dt*udk*1i*(k/l0)*(lam-1.0)*(b0*bky)+thb2*dt*eta/mu0*(k/l0)^2*(lam-1.0)*(b0*bky))/b0;
byrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (dt*(-b0*cs*1i*(k/l0)*lam*(v0*uky)+b0*cs*1i*(k/l0)*lam*(v0*uky)-udk*1i*(k/l0)*(b0*bky)-eta/mu0*(k/l0)^2*(b0*bky)))/b0;
bzlhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) ((lam-1.0)*(b0*bkz)+thb1*dt*udk*1i*(k/l0)*(lam-1.0)*(b0*bkz)+thb2*dt*eta/mu0*(k/l0)^2*(lam-1.0)*(b0*bkz))/b0;
bzrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2) (dt*(-b0*sn*1i*(k/l0)*lam*(v0*uky)+b0*cs*1i*(k/l0)*lam*(v0*ukz)-udk*1i*(k/l0)*(b0*bkz)-eta/mu0*(k/l0)^2*(b0*bkz)))/b0;


pmat1=zeros(nq,nq);
massmat=zeros(nq,nq);
pmat1stf0=zeros(nq,nq);
pmatsingtestpart2=double(zeros(nq,nq));
pmatsingtestpart22=double(zeros(nq,nq));
xq_c2=xq_c;
xq_c2(1)=xq_c2(1)+10^-15;
xq_c2(cquad)=xq_c2(cquad)-10^-15;
for i=1:nc
    for j=1:cquad
      pmat1=pmat1+wq_c(j)*(P(xq_c(j)+(i-1))*jaqarr(j+(i-1)*cquad)).'*P(xq_c(j)+(i-1))*xiqarr(j+(i-1)*cquad);
      massmat=massmat+wq_c(j)*(P(xq_c(j)+(i-1))*jaqarr(j+(i-1)*cquad)).'*P(xq_c(j)+(i-1));
      pmat1stf0=pmat1stf0+wq_c(j)*(P(xq_c(j)+(i-1))).'*(Pdiff(xq_c2(j)+(i-1)))*(1-xiqarr(j+(i-1)*cquad)^2)/2;
      pmatsingtestpart2=pmatsingtestpart2+wq_c(j)*(Pdiff(xq_c2(j)+(i-1))).'*(Pdiff(xq_c2(j)+(i-1)))*ptchqarr(j+(i-1)*cquad);
      pmatsingtestpart22=pmatsingtestpart22+wq_c(j)*(myleg(0:nq-1,xiqarr(j+(i-1)*cquad),true)).'*(Pdiff(xq_c2(j)+(i-1)))*ptchqarr(j+(i-1)*cquad);
    end
end

smatnonsingtest=@(fk,j) (fk(:,j)*2.*sq(j)-fk(:,j)*4.*sq(j).^3+fk*fppmat(:,j).*sq(j)).*chi(sq(j))./(sq(j).^2);
smatsingtestpart1=@(fk,j) (fk*fpmat(:,j).*sq(j)+fk(:,j)*2.*sq(j).^2).*(erf(sq(j))-chi(sq(j)))./(sq(j).^3);
smatsingtestpart2nol=@(fk,j) fk(:,j).*(erf(sq(j))-chi(sq(j)))./(2*sq(j).^3);
smatnonsingtest_kern=(eye(nsp,nsp)*2.*sq-eye(nsp,nsp)*4.*sq.^3+fppmat.*sq).*chi(sq)./(sq.^2);
smatsingtestpart1_kern=(fpmat.*sq+eye(nsp,nsp)*2.*sq.^2).*(erf(sq)-chi(sq))./(sq.^3);
smatsingtestpart2nol_kern=eye(nsp,nsp).*(erf(sq)-chi(sq))./(2*sq.^3);


smatnonsingtest0=(f0.*2.*sq-f0.*4.*sq.^3+f0pp.*sq).*chi(sq)./(sq.^2);
smatsingtestpart10=(f0p.*sq+f0.*2.*sq.^2).*(erf(sq)-chi(sq))./(sq.^3);
smatsingtestpart2nol0=f0.*(erf(sq)-chi(sq))./(2.*sq.^3);
smatfield1f=double(zeros(nsp,nsp,ncolleg)); 
smatfield2f=double(zeros(nsp,nsp,ncolleg)); 
kernel1=double(zeros(nsp,nsp,nq,nq));
kernel2=double(zeros(nsp,nsp,nq,nq));
kernel1_leg=double(zeros(nsp,nsp,nq,nq));
kernel2_leg=double(zeros(nsp,nsp,nq,nq));
matfield1f0=double(zeros(nq,nsp)); matfield2f0=double(zeros(nq,nsp));

collsvec1=exp(-sq.^2).*sq.^2;
collsvec2=exp(-sq.^2).*sq.^4;

mf01= @(l,vbar) vbar.^1.5.*(vbar).^(l+0.5);
mf02= @(l,vbar) vbar.^1.5.*(1./vbar).^(l+0.5);
mf1= @(l,vbar) 1/2*(1+vbar.^2).*(vbar.^(3/2).*vbar.^(l+0.5))+1/2*(1-vbar.^2).*(-1).*vbar.^(3/2).*(l+0.5).*vbar.^(l+0.5)-(1/4).*vbar.^(5/2).*...
            (vbar.^(l+1.5)./(2*l+3)-vbar.^(l-0.5)./(2*l-1));
mf2= @(l,vbar) 1/2*(1+vbar.^2).*(vbar.^(3/2).*(1./vbar).^(l+0.5))+1/2.*(1-vbar.^2)*(1).*vbar.^(3/2).*(l+0.5).*(1./vbar).^(l+0.5)-(1/4)*vbar.^(5/2).*...
            ((1./vbar).^(l+1.5)./(2*l+3)-(1./vbar).^(l-0.5)./(2*l-1));
for i=1:nsp
  smatfield1=double(zeros(nvbar12+nvbar12+nvbar3,ncolleg));
  smatfield2=double(zeros(nvbar12+nvbar12+nvbar3,ncolleg));
  for j=0:ncolleg-1
      smatfield1(1:nvbar12,j+1)=2*npi*mf01(j,vbar1q.');
      smatfield1(nvbar12+1:2*nvbar12,j+1)=2*npi*mf02(j,vbar2q.');
      if(do_domain_3(i))
        smatfield1(2*nvbar12+1:2*nvbar12+nvbar3,j+1)=2*npi*mf02(j,vbar3q(i,:).');
      end
      smatfield2(1:nvbar12,j+1)=2*npi*mf1(j,vbar1q.');
      smatfield2(nvbar12+1:2*nvbar12,j+1)=2*npi*mf2(j,vbar2q.');
      if(do_domain_3(i))
        smatfield2(2*nvbar12+1:2*nvbar12+nvbar3,j+1)=2*npi*mf2(j,vbar3q(i,:).');
      end
  end
  smatfield1f(:,i,:)=squeeze(Lvbarmat(:,i,:))*smatfield1;
  smatfield2f(:,i,:)=squeeze(Lvbarmat(:,i,:))*smatfield2;
end

smatfield1f(abs(smatfield1f)<10^-14)=0;
smatfield2f(abs(smatfield2f)<10^-14)=0;
Pcoup1=eye(ncolleg,nq);
Pcoup2=Pcoup;
nmin=min(nq,ncolleg);
for i=1:nmin
    Pcoup1(i,i)=Pcoup1(i,i)*2.0/(2*(i-1)+1);
end
for i=1:ncolleg
    lind=i-1;
    Pcoup2(i,:)=Pcoup2(i,:)*(2*lind+1)/2*(lind*(lind+1));
end
disp('form kernels')
for i=1:ncolleg
    kernel1(:,:,:,:)=kernel1(:,:,:,:)+squeeze(tensorprod(tensorprod(smatfield1f(:,:,i),Pcoup(i,:)),Pcoup(i,:)));
    kernel2(:,:,:,:)=kernel2(:,:,:,:)+squeeze(tensorprod(tensorprod(smatfield2f(:,:,i),Pcoup(i,:)),Pcoup(i,:)));
    kernel1_leg(:,:,:,:)=kernel1_leg(:,:,:,:)+squeeze(tensorprod(tensorprod(smatfield1f(:,:,i),Pcoup1(i,:)),Pcoup(i,:)));
    kernel2_leg(:,:,:,:)=kernel2_leg(:,:,:,:)+squeeze(tensorprod(tensorprod(smatfield2f(:,:,i),Pcoup1(i,:)),Pcoup(i,:)));
end

massmatinv=inv(massmat);
kernel1_momn1=double(zeros(nsp,nsp,nq));
kernel2_momn1=double(zeros(nsp,nsp,nq));
kernel1_momn=double(zeros(nsp,nq));
kernel2_momn=double(zeros(nsp,nq));
kernel1_momu1=double(zeros(nsp,nsp,nq));
kernel2_momu1=double(zeros(nsp,nsp,nq));
kernel1_momu=double(zeros(nsp,nq));
kernel2_momu=double(zeros(nsp,nq));
kernel1_momt1=double(zeros(nsp,nsp,nq));
kernel2_momt1=double(zeros(nsp,nsp,nq));
kernel1_momt=double(zeros(nsp,nq));
kernel2_momt=double(zeros(nsp,nq));
masstmp=Pcoup*massmatinv;

for i=1:nq
  kernel1_momn1=kernel1_momn1+reshape(squeeze(tensorprod(masstmp(0+1,i),permute(kernel1(:,:,i,:),[3 2 1 4]))),[nsp nsp nq]);
  kernel2_momn1=kernel2_momn1+reshape(squeeze(tensorprod(masstmp(0+1,i),permute(kernel2(:,:,i,:),[3 2 1 4]))),[nsp nsp nq]);
  kernel1_momu1=kernel1_momu1+reshape(squeeze(tensorprod(masstmp(1+1,i),permute(kernel1(:,:,i,:),[3 2 1 4]))),[nsp nsp nq]);
  kernel2_momu1=kernel2_momu1+reshape(squeeze(tensorprod(masstmp(1+1,i),permute(kernel2(:,:,i,:),[3 2 1 4]))),[nsp nsp nq]);
  kernel1_momt1=kernel1_momt1+reshape(squeeze(tensorprod(masstmp(0+1,i),permute(kernel1(:,:,i,:),[3 2 1 4]))),[nsp nsp nq]);
  kernel2_momt1=kernel2_momt1+reshape(squeeze(tensorprod(masstmp(0+1,i),permute(kernel2(:,:,i,:),[3 2 1 4]))),[nsp nsp nq]);
end
for j=1:nsp
  kernel1_momn=kernel1_momn+squeeze(kernel1_momn1(j,:,:)*collsvec1(j)*sq(j)^2*wq(j));
  kernel2_momn=kernel2_momn+squeeze(kernel2_momn1(j,:,:)*collsvec2(j)*sq(j)^2*wq(j));
  kernel1_momu=kernel1_momu+squeeze(kernel1_momu1(j,:,:)*collsvec1(j)*sq(j)^3*wq(j));
  kernel2_momu=kernel2_momu+squeeze(kernel2_momu1(j,:,:)*collsvec2(j)*sq(j)^3*wq(j));
  kernel1_momt=kernel1_momt+squeeze(kernel1_momt1(j,:,:)*collsvec1(j)*sq(j)^4*wq(j));
  kernel2_momt=kernel2_momt+squeeze(kernel2_momt1(j,:,:)*collsvec2(j)*sq(j)^4*wq(j));
end

svecfield3=4*npi*exp(-sq.^2);

disp('dot kernels')
parfor (i=1:nq,8)
    for j=1:nsp
        matfield1f0(i,j)=collsvec1(j)*sum(f0(:,:).'.*squeeze(kernel1(:,j,i,:)),'all');
        matfield2f0(i,j)=collsvec2(j)*sum(f0(:,:).'.*squeeze(kernel2(:,j,i,:)),'all');
    end
end
matfield1fold=@(fk1,i,j) matfieldf(fk1,kernel1,collsvec1,nq,nsp,i,j);
matfield1f=@(fk1,i,j) reshape(squeeze(sum(fk1(:,:).'.*(permute(kernel1(:,j,i,:),[1 4 3 2])),[1 2])),[numel(i) numel(j)]).*collsvec1(j);
matfield2fold=@(fk1,i,j) matfieldf(fk1,kernel2,collsvec2,nq,nsp,i,j);
matfield2f=@(fk1,i,j) reshape(squeeze(sum(fk1(:,:).'.*(permute(kernel2(:,j,i,:),[1 4 3 2])),[1 2])),[numel(i) numel(j)]).*collsvec2(j);

matfield1f_leg=@(fk1,i,j) reshape(squeeze(sum(fk1(:,:).'.*(permute(kernel1_leg(:,j,i,:),[1 4 3 2])),[1 2])),[numel(i) numel(j)]).*collsvec1(j);
matfield2f_leg=@(fk1,i,j) reshape(squeeze(sum(fk1(:,:).'.*(permute(kernel2_leg(:,j,i,:),[1 4 3 2])),[1 2])),[numel(i) numel(j)]).*collsvec2(j);

matfield1f_momn=@(fk1) squeeze(sum(fk1(:,:).'.*kernel1_momn(:,:),[1 2]));
matfield2f_momn=@(fk1) squeeze(sum(fk1(:,:).'.*kernel2_momn(:,:),[1 2]));
matfield1f_momu=@(fk1) squeeze(sum(fk1(:,:).'.*kernel1_momu(:,:),[1 2]));
matfield2f_momu=@(fk1) squeeze(sum(fk1(:,:).'.*kernel2_momu(:,:),[1 2]));
matfield1f_momt=@(fk1) squeeze(sum(fk1(:,:).'.*kernel1_momt(:,:),[1 2]));
matfield2f_momt=@(fk1) squeeze(sum(fk1(:,:).'.*kernel2_momt(:,:),[1 2]));

pvec0=Pcoup(0+1,:);
pvec1=Pcoup(1+1,:);
pvec2=Pcoup(2+1,:);

disp('end kernels')

feqlhs=cell(nq,nsp);
feqrhs=cell(nq,nsp);

masstmp2=Pcoup*massmatinv*pmatsingtestpart2;
masstmp22=massmatinv*pmatsingtestpart2;
tempeye=eye(nsp,nsp);

Cf=@(lam,k,fk,i,j) (nuii*(massmat(i,:)*smatnonsingtest(fk,j)+massmat(i,:)*smatsingtestpart1(fk,j)-pmatsingtestpart2(i,:)*smatsingtestpart2nol(fk,j))+...
        nuii/npi^1.5*(-2.0*(matfield1f((fk),i,j))+2.0*(matfield2f((fk),i,j))+massmat(i,:)*((fk(:,j)).*svecfield3(j))));
Cf_kern=@(i,j,ip,jp) ((massmat(i,ip)*smatnonsingtest_kern(jp,j)+massmat(i,ip)*smatsingtestpart1_kern(jp,j)-pmatsingtestpart2(i,ip)*smatsingtestpart2nol_kern(jp,j))+...
        1.0/npi^1.5*(-2.0*(kernel1(jp,j,i,ip)*collsvec1(j))+2.0*(kernel2(jp,j,i,ip)*collsvec2(j))+massmat(i,ip)*(tempeye(jp,j).*svecfield3(j))));
Cf_mod=@(lam,k,fk,i,j) (nuii*(smatnonsingtest(fk,j)+smatsingtestpart1(fk,j)-masstmp22(i,:)*smatsingtestpart2nol(fk,j))+...
        nuii/npi^1.5*(fk(:,j).*svecfield3(j)));

Cf_momn=@(lam,k,fk) ...
        nuii/npi^1.5*(-2.0*(matfield1f_momn(fk))+2.0*(matfield2f_momn(fk)));
Cf_momu=@(lam,k,fk) ...
        nuii/npi^1.5*(-2.0*(matfield1f_momu(fk))+2.0*(matfield2f_momu(fk)));
Cf_momt=@(lam,k,fk) ...
        nuii/npi^1.5*(-2.0*(matfield1f_momt(fk))+2.0*(matfield2f_momt(fk)));


chiarr=chi(sq);
erfarr=erf(sq);

collmod1=(nuii*(((2.*sq-4.*sq.^3).*chiarr./(sq.^2))+((2.*sq.^2).*(erfarr-chiarr)./(sq.^3)))+...
        nuii/npi^1.5*(svecfield3));

collmod=@(fk) (nuii*(((fk*fppmat.*sq).*chiarr./(sq.^2))+((fk*fpmat.*sq).*(erfarr-chiarr)./(sq.^3))-masstmp22*(fk.*(erfarr-chiarr)./(2*sq.^3)))+fk.*collmod1);

doCm=1.0;
if(doCm>0)
    Cmomn= @(lam,k,fk) (2.0*npi*(2.0*kb*t/mi)^(3.0/2)*(Pcoup(0+1,:)*sum(collmod(fk).*sq.^2.*wq,2)+Cf_momn(lam,k,fk)));
    Cmomnold= @(lam,k,fk) (2.0*npi*(2.0*kb*t/mi)^(3.0/2)*sum((Pcoup(0+1,:)*massmatinv*Cf(lam,k,fk,1:nq,1:nsp)).*sq.^2.*wq));
    Cmomndel= @(lam,k,fk) (2.0*npi*(2.0*kb*t/mi)^(3.0/2)*(lam-1.0)*(Pcoup(0+1,:)*sum(collmod(fk).*sq.^2.*wq,2)+Cf_momn(lam,k,fk)));
    Cmomndelold= @(lam,k,fk) (2.0*npi*(2.0*kb*t/mi)^(3.0/2)*sum((lam-1.0)*(Pcoup(0+1,:)*massmatinv*Cf(lam,k,fk,1:nq,1:nsp)).*sq.^2.*wq));
    Cmomu= @(lam,k,fk) (2.0*npi*mi*(2.0*kb*t/mi)^(4.0/2)*(Pcoup(1+1,:)*sum(collmod(fk).*sq.^3.*wq,2)+Cf_momu(lam,k,fk)));
    Cmomuold= @(lam,k,fk) (2.0*npi*mi*(2.0*kb*t/mi)^(4.0/2)*sum((Pcoup(1+1,:)*massmatinv*Cf(lam,k,fk,1:nq,1:nsp)).*sq.^3.*wq));
    Cmomudel= @(lam,k,fk) (2.0*npi*mi*(2.0*kb*t/mi)^(4.0/2)*(lam-1.0)*(Pcoup(1+1,:)*sum(collmod(fk).*sq.^3.*wq,2)+Cf_momu(lam,k,fk)));
    Cmomudelold= @(lam,k,fk) (2.0*npi*mi*(2.0*kb*t/mi)^(4.0/2)*sum((lam-1.0)*(Pcoup(1+1,:)*massmatinv*Cf(lam,k,fk,1:nq,1:nsp)).*sq.^3.*wq));
    Cmomt= @(lam,k,fk) (npi*mi*(2.0*kb*t/mi)^(5.0/2)*(Pcoup(0+1,:)*sum(collmod(fk).*sq.^4.*wq,2)+Cf_momt(lam,k,fk)));
    Cmomtold= @(lam,k,fk) (npi*mi*(2.0*kb*t/mi)^(5.0/2)*sum((Pcoup(0+1,:)*massmatinv*Cf(lam,k,fk,1:nq,1:nsp)).*sq.^4.*wq));
    Cmomtdel= @(lam,k,fk) (npi*mi*(2.0*kb*t/mi)^(5.0/2)*(lam-1.0)*(Pcoup(0+1,:)*sum(collmod(fk).*sq.^4.*wq,2)+Cf_momt(lam,k,fk)));
    Cmomtdelold= @(lam,k,fk) (npi*mi*(2.0*kb*t/mi)^(5.0/2)*sum((lam-1.0)*(Pcoup(0+1,:)*massmatinv*Cf(lam,k,fk,1:nq,1:nsp)).*sq.^4.*wq));
else
    Cmomn= @(lam,k,fk) 0.0;
    Cmomndel= @(lam,k,fk) 0.0;
    Cmomu= @(lam,k,fk) 0.0;
    Cmomudel= @(lam,k,fk) 0.0;
    Cmomt= @(lam,k,fk) 0.0;
    Cmomtdel= @(lam,k,fk) 0.0;
end

for i=1:nq
    for j=1:nsp
diffart=00;
% momadcenter1=0.5;
% momadcenter2=0.5;
% cmcenter1=0.5;
% cmcenter2=0.5;
% cmcenter3=0.5;
momadcenter1=1.0;
momadcenter2=1.0;
 cmcenter1=1.0;
 cmcenter2=1.0;
 cmcenter3=1.0;
diffart2center=1.0;
diffart2=1*10^-8;
%diffart2=0.0;
domomad=1.0;
feqlhs{i,j}= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4) ((lam-1.0)*massmat(i,:)*(fm1*fk(:,j))+dt*(thf1*1i*(k/l0)*vt*cs*(lam-1.0)*pmat1(i,:)*(fm1*fk(:,j))*sq(j)-fm1*(vt*zeff/n/(kb*t)*(thf2*2.0/3*1i*(k/l0)*cs*pipardel(lam,(k/l0),(fm1*fk))-diffart2center*diffart2*(k/l0)^2*(momudel(lam,(k/l0),(fm1*fk)))*(3.5-sq(j)^2)-cmcenter2*Cmomudel(lam,(k/l0),(fm1*fk))*(3.5-sq(j)^2)+momadcenter2*domomad*2/3*momtdel(lam,(k/l0),(fm1*fk))*1i*(k/l0)*cs*(3.5-sq(j)^2))*pvec1(i)*sq(j)*exp(-sq(j)^2)+...
    2.0/3*zeff/n/(kb*t)*(thf3*1i*(k/l0)*cs*qpardel(lam,(k/l0),(fm1*fk))-diffart2center*diffart2*(k/l0)^2*(momtdel(lam,(k/l0),(fm1*fk)))-cmcenter3*Cmomtdel(lam,(k/l0),(fm1*fk)))*pvec0(i)*(sq(j)^2-1.5)*exp(-sq(j)^2)-diffart2center*diffart2*(k/l0)^2*(momndel(lam,(k/l0),(fm1*fk)))/(n/zeff)*pvec0(i)*(2.5-sq(j)^2)*exp(-sq(j)^2)-cmcenter1*1.0/(n/zeff)*Cmomndel(lam,(k/l0),(fm1*fk))*pvec0(i)*(2.5-sq(j)^2)*exp(-sq(j)^2)+momadcenter1*domomad*1.0/(mi*n/zeff)*1i*(k/l0)*cs*momudel(lam,(k/l0),(fm1*fk))*pvec0(i)*(2.5-sq(j)^2)*exp(-sq(j)^2)))-dt*(thfc1*nuii*(lam-1.0)*(massmat(i,:)*smatnonsingtest(fm1*fk,j)+massmat(i,:)*smatsingtestpart1(fm1*fk,j)-pmatsingtestpart2(i,:)*smatsingtestpart2nol(fm1*fk,j))+...
    thfc2*nuii/npi^1.5*(lam-1.0)*(-2.0*(matfield1f((fm1*fk),i,j))+2.0*(matfield2f((fm1*fk),i,j))+massmat(i,:)*(fm1*fk(:,j))*svecfield3(j)))+diffart*thf4*dt*(k/l0)^2*(lam-1.0)*massmat(i,:)*(fm1*fk(:,j)))/fm1;
feqrhs{i,j}=@(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk,thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4) (-dt*1i*(k/l0)*vt*cs*pmat1(i,:)*(fm1*fk(:,j))*sq(j)+dt*(nuii*(massmat(i,:)*smatnonsingtest(fm1*fk,j)+massmat(i,:)*smatsingtestpart1(fm1*fk,j)-pmatsingtestpart2(i,:)*smatsingtestpart2nol(fm1*fk,j))+...
    nuii/npi^1.5*(-2.0*(matfield1f((fm1*fk),i,j))+2.0*(matfield2f((fm1*fk),i,j))+massmat(i,:)*(fm1*fk(:,j))*svecfield3(j)))+dt*fm1*(1i*(k/l0)*vt/t*cs*(thft*(lam-1.0)+1.0)*(t*tk)*pvec1(i)*(sq(j)*(2.5-sq(j)^2)*exp(-sq(j)^2))+vt*zeff/n/(kb*t)*(2.0/3*1i*(k/l0)*cs*pipar(lam,(k/l0),(fm1*fk))-diffart2*(k/l0)^2*(momu(lam,(k/l0),(fm1*fk)))*(3.5-sq(j)^2)-Cmomu(lam,(k/l0),(fm1*fk))*(3.5-sq(j)^2)+domomad*2/3*momt(lam,(k/l0),(fm1*fk))*1i*(k/l0)*cs*(3.5-sq(j)^2))*pvec1(i)*sq(j)*exp(-sq(j)^2)+...
    2.0/3*zeff/n/(kb*t)*(1i*(k/l0)*cs*qpar(lam,(k/l0),(fm1*fk))-diffart2*(k/l0)^2*(momt(lam,(k/l0),(fm1*fk)))-Cmomt(lam,(k/l0),(fm1*fk)))*pvec0(i)*(sq(j)^2-1.5)*exp(-sq(j)^2)+2.0/3*(1i*(k/l0)*(thfu*(lam-1.0)+1.0)*(v0*uky)-1i*(k/l0)*3.0*cs*(thfu*(lam-1.0)+1.0)*(cs*(v0*uky)+sn*(v0*ukz)))*pvec2(i)*(sq(j)^2*exp(-sq(j)^2))-diffart2*(k/l0)^2*(momn(lam,(k/l0),(fm1*fk)))/(n/zeff)*pvec0(i)*(2.5-sq(j)^2)*exp(-sq(j)^2)-1.0/(n/zeff)*Cmomn(lam,(k/l0),(fm1*fk))*pvec0(i)*(2.5-sq(j)^2)*exp(-sq(j)^2)+domomad*1.0/(mi*n/zeff)*1i*(k/l0)*cs*momu(lam,(k/l0),(fm1*fk))*pvec0(i)*(2.5-sq(j)^2)*exp(-sq(j)^2))+...
    dt*(vt*(1i*(k/l0)*cs)*(thfb*(lam-1.0)+1.0)*(cs*(b0*bky)+sn*(b0*bkz))/b0*pmat1stf0(i,:)*f0(:,j)*sq(j)-vt*(1i*(k/l0)*cs)*(thfn*(lam-1.0)+1.0)*(n/zeff*nk)/n*pmat1stf0(i,:)*f0(:,j)/sq(j))-...
    dt*(vt/2.0*(1i*(k/l0)*cs)*(thfn*(lam-1.0)+1.0)*(n/zeff*nk)/n*pmat1(i,:)*f0p(:,j)-vt/2.0*(1i*(k/l0)*cs)*(thft*(lam-1.0)+1.0)*(t*tk)/t*pmat1(i,:)*f0p(:,j)*sq(j)^2-do_delt*1.0/2*(lam-1.0)*(t*tk)/t/dt*massmat(i,:)*f0p(:,j)*sq(j))+...
    dt*fm1*(vt*zeff/n/(kb*t)*(5.0/3*pipar0/t*1i*(k/l0)*cs*(thft*(lam-1.0)+1.0)*(t*tk)-pipar0*(1i*(k/l0)*cs)*(thfb*(lam-1.0)+1.0)*(cs*(b0*bky)+sn*(b0*bkz))/b0)*pvec1(i)*sq(j)*exp(-sq(j)^2)+...
    2.0/3*zeff/n/(kb*t)*(-qpar0*(1i*(k/l0)*cs)*(thfb*(lam-1.0)+1.0)*(cs*(b0*bky)+sn*(b0*bkz))/b0+3.0*qpar0/t*(1i*(k/l0)*cs)*(thft*(lam-1.0)+1.0)*(t*tk))*pvec0(i)*(sq(j)^2-1.5)*exp(-sq(j)^2))-diffart*dt*(k/l0)^2*massmat(i,:)*(fm1*fk(:,j)))/fm1;

    end
end
% %these terms are needed only if want drive term on rhs of equation
%    dt*(nuii+nuii1)*((massmat*smatnonsingtest0+massmat*smatsingtestpart10-pmatsingtestpart2*smatsingtestpart2nol0)+1.0/npi^1.5*(-2.0*matfield1f0+2.0*matfield2f0+massmat*f0.*svecfield3));

M2f2=@(thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4) M2f(thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4,nq,nsp,vxlhs,vxrhs,vylhs,vyrhs,vzlhs,vzrhs,bxlhs,bxrhs,bylhs,byrhs,bzlhs,bzrhs,tlhs,trhs,telhs,terhs,nlhs,nrhs,feqlhs,feqrhs);

krange=(0:40*npi/200.0:40*npi);
lamstorek=cell(1,numel(krange));
eigvecstorek=cell(1,numel(krange));
eigvalstorek=cell(1,numel(krange));
omegastorek=cell(1,numel(krange));
kstorek=cell(1,numel(krange));

do_opt=false;
if(do_opt)
    options = optimoptions('fmincon','Display','iter');
    rng('shuffle')
    for i=1:20
        x0=rand(1,19)
        x=fmincon(@(xin) mycost(M1lamf2,M1othf2,krange,dt,t0,xin),...
        x0,[],[],[],[],zeros(1,19),ones(1,19),[],options);
        %x=fmincon(@(xin) mycost(M1lamf2,M1othf2,krange,dt,t0,xin),...
        %    x0,[],[],[],[],zeros(1,19),ones(1,19));
        x
        mycost(M1lamf2,M1othf2,krange,dt,t0,x)
    end
end

x=[0.5 0.5 0.5 0.5 0.5 1.0 0.5 1.0 1.0 0.5 0.5 1.0 0.0 0.0 1.0 1.0 0.0]; %theta_1 centerings
%x=[0.5 0.5 0.5 0.5 0.5 1.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.5 0.5 0.5]; %theta_2 centerings
%x=[0.5 0.5 0.5 0.5 0.5 1.0 0.5 1.0 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5]; %all centerings 0.5
%x
x=[x 1.0*ones(1,2) 1.0]; % %theta_cii = 1.0
 %x=[x 0.5*ones(1,2) 1.0]; %theta_cii = 0.5

xt=num2cell(x);
tic
disp('matrices')
[Mlamk0,Mlamk1,Mlamk2,Mothk0,Mothk1,Mothk2]=M2f2(xt{:});
disp('done')
toc
Mlam=@(k) Mlamk0+Mlamk1*k+Mlamk2*k^2;
Moth=@(k) Mothk0+Mothk1*k+Mothk2*k^2;

tst=cell(1,numel(krange));
parfor i=1:numel(krange)
  lamstorek{i}=eig(-inv(Mlam(krange(i)))*Moth(krange(i)));
  [eigvecstorek{i},eigvalstorek{i}]=eig(-inv(Mlam(krange(i)))*Moth(krange(i)));
  omegastorek{i}=-log(lamstorek{i})/1i/(dt/t0);
  eigvalstorek{i}=-log(eigvalstorek{i})/1i/(dt/t0);
  kstorek{i}=krange(i)*ones(27,1);
  tst{i}=max(imag(omegastorek{i}));
end
max_growth_rate_fluidkinetic=max(cell2mat(tst));
max_growth_rate_fluidkinetic

figure(1)
scatter(krange,real(cell2mat(omegastorek)),10,'k','filled')
%scatter(krange,real(cell2mat(omegastorek)),200,'k','LineWidth',3.75)
ax1=gca;
ax1.FontSize=35 ;
ax1.LineWidth=5;
xlabel('$k\cdot d_i$','Interpreter','latex', 'FontSize', 55);
ylabel('Re$(\omega)$ $\cdot\ \Omega_i^{-1}$','Interpreter','latex', 'FontSize', 55);
xlim([0 126])
set(gcf,'Position',[500 500 1200 900]);
figure(2)
scatter(krange,imag(cell2mat(omegastorek)),10,'k','filled') %40 for slides
%scatter(krange,imag(cell2mat(omegastorek)),100,'k','LineWidth',1.75)  %vary for full zoom to 3.75, 200% 
hold on
ax2=gca;
ax2.FontSize=35 ;
ax2.LineWidth=5;
xlabel('$k\cdot d_i$','Interpreter','latex', 'FontSize', 55);
ylabel('Im$(\omega)$ $\cdot\ \Omega_i^{-1}$','Interpreter','latex', 'FontSize', 55);
xlim([0 126])
set(gcf,'Position',[500 500 1200 900]);
%ylim([4*-10^-16 4*10^-16])
%return;

syms lam k ukx uky ukz bkx bky bkz tk tek nk fk;
%clear thv1 thn tht1 tht2 thte1 thte2 thb1 thb2 thft thfn thfb thfu ft_simul fu_simul thf1 thf2 thf3 thfc1 thfc2;
thv2=0.5; iso_visc=0.0; kpari=0.0^3;kpare=0.0;eta=0.0;
thv1=0.5;thn=0.5;tht1=0.5;tht2=0.5;thte1=0.5;thte2=0.5;thb1=0.5;thb2=0.5;

vxlhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (mi*n/zeff*((lam-1.0)*(v0*ukx)+thv1*dt*udk*1i*(k/l0)*(lam-1.0)*(v0*ukx))-dt^2*c0*(-(k/l0)^2)*(b0^2/mu0*(cs^2*(lam-1.0)*(v0*ukx)))-thv2*dt*iso_visc*mi*n/zeff*(-(k/l0)^2)*(lam-1.0)*(v0*ukx))/(mi*n/zeff*v0);
vxrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (-mi*n/zeff*dt*udk*1i*(k/l0)*(v0*ukx)+dt/mu0*1i*(k/l0)*((b0*bkx)*b0*cs)+dt*iso_visc*mi*n/zeff*(-(k/l0)^2)*(v0*ukx))/(mi*n/zeff*v0);
vylhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (mi*n/zeff*((lam-1.0)*(v0*uky)+thv1*dt*udk*1i*(k/l0)*(lam-1.0)*(v0*uky))-dt^2*c0*(-(k/l0)^2)*(b0^2/mu0*(-cs*sn*(lam-1.0)*(v0*ukz)+ ...
    sn^2*(lam-1.0)*(v0*uky))+kb*gam*(n/zeff*t+n*te)*(lam-1.0)*(v0*uky))-thv2*dt*iso_visc*mi*n/zeff*(-(k/l0)^2)*(lam-1.0)*((v0*uky)+1.0/3*(v0*uky)))/(mi*n/zeff*v0);
vyrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (-mi*n/zeff*dt*udk*1i*(k/l0)*(v0*uky)+dt/mu0*1i*(k/l0)*((b0*bkz)*b0*(-sn))-dt*1i*(k/l0)*kb*(n/zeff*(t*tk)+t*(n/zeff*nk)/zeff+n*(t*tek)+te*(n/zeff*nk))+ ... 
   dt*iso_visc*mi*n/zeff*(-(k/l0)^2)*((v0*uky)+1.0/3*(v0*uky)))/(mi*n/zeff*v0);
vzlhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (mi*n/zeff*((lam-1.0)*(v0*ukz)+thv1*dt*udk*1i*(k/l0)*(lam-1.0)*(v0*ukz))-dt^2*c0*(-(k/l0)^2)*(b0^2/mu0*(cs^2*(lam-1.0)*(v0*ukz)- ...
    cs*sn*(lam-1.0)*(v0*uky)))-thv2*dt*iso_visc*mi*n/zeff*(-(k/l0)^2)*(lam-1.0)*(v0*ukz))/(mi*n/zeff*v0);
vzrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (-mi*n/zeff*dt*udk*1i*(k/l0)*(v0*ukz)+dt/mu0*1i*(k/l0)*((b0*bkz)*b0*cs)+dt*iso_visc*mi*n/zeff*(-(k/l0)^2)*(v0*ukz))/(mi*n/zeff*v0);

tlhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (1.5*n*kb/zeff*((lam-1.0)*(t*tk)+tht1*dt*udk*1i*(k/l0)*(lam-1.0)*(t*tk))+tht2*dt*kpari*cs^2*(k/l0)^2*kb*(lam-1.0)*(t*tk))/(1.5*n*kb/zeff*t);
trhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (-1.5*n*kb/zeff*dt*udk*1i*(k/l0)*(t*tk)-dt*n*t*kb/zeff*1i*(k/l0)*lam*(v0*uky)-dt*kpari*cs^2*(k/l0)^2*kb*(t*tk))/(1.5*n*kb/zeff*t);

nlhs= @(lam,k,ukx,uky,ukz,nk) ((lam-1.0)*(n/zeff*nk)+thn*dt*udk*1i*(k/l0)*(lam-1.0)*(n/zeff*nk))/(n/zeff);
nrhs= @(lam,k,ukx,uky,ukz,nk) (-dt*udk*1i*(k/l0)*(n/zeff*nk)-dt*n*1i*(k/l0)*lam*(v0*uky))/(n/zeff);

telhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (1.5*n*kb*((lam-1.0)*(t*tek)+thte1*dt*udk*1i*(k/l0)*(lam-1.0)*(t*tek))+thte2*dt*kpare*cs^2*(k/l0)^2*kb*(lam-1.0)*(t*tek))/(1.5*n*kb*t);
terhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (-1.5*n*kb*dt*udk*1i*(k/l0)*(t*tek)-dt*n*te*kb*1i*(k/l0)*lam*(v0*uky)-dt*kpare*cs^2*(k/l0)^2*kb*(t*tek))/(1.5*n*kb*t);

bxlhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) ((lam-1.0)*(b0*bkx)+thb1*dt*udk*1i*(k/l0)*(lam-1.0)*(b0*bkx)+thb2*dt*eta/mu0*(k/l0)^2*(lam-1.0)*(b0*bkx))/b0;
bxrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (dt*(b0*cs*1i*(k/l0)*lam*(v0*ukx)-udk*1i*(k/l0)*(b0*bkx)-eta/mu0*(k/l0)^2*(b0*bkx)))/b0;
bylhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) ((lam-1.0)*(b0*bky)+thb1*dt*udk*1i*(k/l0)*(lam-1.0)*(b0*bky)+thb2*dt*eta/mu0*(k/l0)^2*(lam-1.0)*(b0*bky))/b0;
byrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (dt*(-b0*cs*1i*(k/l0)*lam*(v0*uky)+b0*cs*1i*(k/l0)*lam*(v0*uky)-udk*1i*(k/l0)*(b0*bky)-eta/mu0*(k/l0)^2*(b0*bky)))/b0;
bzlhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) ((lam-1.0)*(b0*bkz)+thb1*dt*udk*1i*(k/l0)*(lam-1.0)*(b0*bkz)+thb2*dt*eta/mu0*(k/l0)^2*(lam-1.0)*(b0*bkz))/b0;
bzrhs= @(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk) (dt*(-b0*sn*1i*(k/l0)*lam*(v0*uky)+b0*cs*1i*(k/l0)*lam*(v0*ukz)-udk*1i*(k/l0)*(b0*bkz)-eta/mu0*(k/l0)^2*(b0*bkz)))/b0;


cvxtmp=coeffs(vxlhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk)-vxrhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk),lam);
clamvx=cvxtmp(2);
cothvx=cvxtmp(1);
cvytmp=coeffs(vylhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk)-vyrhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk),lam);
clamvy=cvytmp(2);
cothvy=cvytmp(1);
cvztmp=coeffs(vzlhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk)-vzrhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk),lam);
clamvz=cvztmp(2);
cothvz=cvztmp(1);
cbxtmp=coeffs(bxlhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk)-bxrhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk),lam);
clambx=cbxtmp(2);
cothbx=cbxtmp(1);
cbytmp=coeffs(bylhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk)-byrhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk),lam);
clamby=cbytmp(2);
cothby=cbytmp(1);
cbztmp=coeffs(bzlhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk)-bzrhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk),lam);
clambz=cbztmp(2);
cothbz=cbztmp(1);
cttmp=coeffs(tlhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk)-trhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk),lam);
clamt=cttmp(2);
cotht=cttmp(1);
ctetmp=coeffs(telhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk)-terhs(lam,k,ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk,fk),lam);
clamte=ctetmp(2);
cothte=ctetmp(1);
cntmp=coeffs(nlhs(lam,k,ukx,uky,ukz,nk)-nrhs(lam,k,ukx,uky,ukz,nk),lam);
clamn=cntmp(2);
cothn=cntmp(1);

[M1lam,M2lam]=equationsToMatrix([clamvx==0.0,clamvy==0.0,clamvz==0.0,clambx==0.0,clamby==0.0,clambz==0.0,clamt==0.0,clamte==0.0,clamn==0.0],[ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk]);
[M1oth,M2oth]=equationsToMatrix([cothvx==0.0,cothvy==0.0,cothvz==0.0,cothbx==0.0,cothby==0.0,cothbz==0.0,cotht==0.0,cothte==0.0,cothn==0.0],[ukx,uky,ukz,bkx,bky,bkz,tk,tek,nk]);

krange=(0:40*npi/200.00:40*npi);
lamstorek2=cell(1,numel(krange));
eigvecstorek2=cell(1,numel(krange));
eigvalstorek2=cell(1,numel(krange));
omegastorek2=cell(1,numel(krange));
kstorek=cell(1,numel(krange));

M1lamf21=matlabFunction(M1lam);
M1othf21=matlabFunction(M1oth);
M1lamf22=@(k) double(M1lamf21(k));
M1othf22=@(k) double(M1othf21(k));

tst=cell(1,numel(krange));
for i=1:numel(krange)
  lamstorek2{i}=eig(-inv(M1lamf22(krange(i)))*M1othf22(krange(i)));
  [eigvecstorek2{i},eigvalstorek2{i}]=eig(-inv(M1lamf22(krange(i)))*M1othf22(krange(i)));
  omegastorek2{i}=-log(lamstorek2{i})/1i/(dt/t0);
  eigvalstorek2{i}=-log(eigvalstorek2{i})/1i/(dt/t0);
  kstorek{i}=krange(i)*ones(27,1);
  tst{i}=max(imag(omegastorek2{i}));
end
max_growth_rate_fluid=max(cell2mat(tst));
max_growth_rate_fluid

figure(3)
scatter(krange,real(cell2mat(omegastorek2)),10,'k','filled')
%scatter(krange,real(cell2mat(omegastorek)),200,'k','LineWidth',3.75)
ax1=gca;
ax1.FontSize=35 ;
ax1.LineWidth=5;
xlabel('$k\cdot d_i$','Interpreter','latex', 'FontSize', 55);
ylabel('Re$(\omega)$ $\cdot\ \Omega_i^{-1}$','Interpreter','latex', 'FontSize', 55);
xlim([0 126])
set(gcf,'Position',[500 500 1200 900]);
figure(4)
scatter(krange,imag(cell2mat(omegastorek2)),10,'k','filled') %40 for slides
%scatter(krange,imag(cell2mat(omegastorek)),100,'k','LineWidth',1.75)  %vary for full zoom to 3.75, 200% 
hold on
ax2=gca;
ax2.FontSize=35 ;
ax2.LineWidth=5;
xlabel('$k\cdot d_i$','Interpreter','latex', 'FontSize', 55);
ylabel('Im$(\omega)$ $\cdot\ \Omega_i^{-1}$','Interpreter','latex', 'FontSize', 55);
xlim([0 126])
set(gcf,'Position',[500 500 1200 900]);

%An older way of doing things.
function [matfield]=matfieldf(fk,kernel,collsvec,nq,nsp,i,j)
%   matfield=double(zeros(nq,nsp));
%   parfor (i=1:nq,8)
%     for j=1:nsp
        matfield=collsvec(j)*sum(fk(:,:).'.*squeeze(kernel(:,j,i,:)),'all');
%     end
%   end
end

% This gross looking function seems to be the fastest way to assemble the
% matrix.
function [Mlamk0,Mlamk1,Mlamk2,Mothk0,Mothk1,Mothk2]=M2f(thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4,nq,nsp,vxlhs,vxrhs,vylhs,vyrhs,vzlhs,vzrhs,bxlhs,bxrhs,bylhs,byrhs,bzlhs,bzrhs,tlhs,trhs,telhs,terhs,nlhs,nrhs,feqlhs,feqrhs)
    nmax=9+nq*nsp;
    Mlamk0=zeros(9+nq*nsp,9+nq*nsp);
    Mlamk1=zeros(9+nq*nsp,9+nq*nsp);
    Mlamk2=zeros(9+nq*nsp,9+nq*nsp);
    Mothk0=zeros(9+nq*nsp,9+nq*nsp);
    Mothk1=zeros(9+nq*nsp,9+nq*nsp);
    Mothk2=zeros(9+nq*nsp,9+nq*nsp);
    invec=cell(1,9+nq*nsp);
    invec2=cell(1,9+nq*nsp);
    for j=1:9+nq*nsp
       invec{j}=num2cell(zeros(1,9));
       invec{j}{end+1}=zeros(nq,nsp);
       invec2{j}=num2cell(zeros(1,4));
       if (j<=3)
           invec2{j}{j}=1;
       elseif (j==9)
           invec2{j}{4}=1;
       end
       if(j<=9)
           invec{j}{j}=1;
       else
           ii=mod((j-9)-1,nq)+1;
           jj=idivide(int32((j-9)-1),nq,'floor')+1;
           invec{j}{10}(ii,jj)=1;
       end
    end
    parfor (i=1:9+nq*nsp,8)
        %disp(i)
      for j=1:nmax
        switch i
            case 1
                    Mlamk0(i,j)= vxlhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vxlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mlamk1(i,j)= (vxlhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vxlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-(vxlhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vxlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mlamk2(i,j)= (vxlhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vxlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+(vxlhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vxlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-(vxlhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vxlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
                    Mothk0(i,j)= (vxlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mothk1(i,j)= ((vxlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-((vxlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mothk2(i,j)= ((vxlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+((vxlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-((vxlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vxrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
            case 2
                    Mlamk0(i,j)= vylhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vylhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mlamk1(i,j)= (vylhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vylhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-(vylhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vylhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mlamk2(i,j)= (vylhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vylhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+(vylhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vylhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-(vylhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vylhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
                    Mothk0(i,j)= (vylhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mothk1(i,j)= ((vylhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-((vylhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mothk2(i,j)= ((vylhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+((vylhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-((vylhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vyrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
            case 3
                    Mlamk0(i,j)= vzlhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vzlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mlamk1(i,j)= (vzlhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vzlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-(vzlhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vzlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mlamk2(i,j)= (vzlhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vzlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+(vzlhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vzlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-(vzlhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(vzlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
                    Mothk0(i,j)= (vzlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mothk1(i,j)= ((vzlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-((vzlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mothk2(i,j)= ((vzlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+((vzlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-((vzlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       vzrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
            case 4
                    Mlamk0(i,j)= bxlhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bxlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mlamk1(i,j)= (bxlhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bxlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-(bxlhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bxlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mlamk2(i,j)= (bxlhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bxlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+(bxlhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bxlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-(bxlhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bxlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
                    Mothk0(i,j)= (bxlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mothk1(i,j)= ((bxlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-((bxlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mothk2(i,j)= ((bxlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+((bxlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-((bxlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bxrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
            case 5
                    Mlamk0(i,j)= bylhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bylhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mlamk1(i,j)= (bylhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bylhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-(bylhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bylhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mlamk2(i,j)= (bylhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bylhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+(bylhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bylhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-(bylhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bylhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
                    Mothk0(i,j)= (bylhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mothk1(i,j)= ((bylhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-((bylhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mothk2(i,j)= ((bylhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+((bylhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-((bylhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       byrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
            case 6
                    Mlamk0(i,j)= bzlhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bzlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mlamk1(i,j)= (bzlhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bzlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-(bzlhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bzlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mlamk2(i,j)= (bzlhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bzlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+(bzlhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bzlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-(bzlhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(bzlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
                    Mothk0(i,j)= (bzlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mothk1(i,j)= ((bzlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-((bzlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mothk2(i,j)= ((bzlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+((bzlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-((bzlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       bzrhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
            case 7
                    Mlamk0(i,j)= tlhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(tlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mlamk1(i,j)= (tlhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(tlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-(tlhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(tlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mlamk2(i,j)= (tlhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(tlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+(tlhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(tlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-(tlhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(tlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
                    Mothk0(i,j)= (tlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mothk1(i,j)= ((tlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-((tlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mothk2(i,j)= ((tlhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+((tlhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-((tlhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       trhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
            case 8
                    Mlamk0(i,j)= telhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(telhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mlamk1(i,j)= (telhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(telhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-(telhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(telhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mlamk2(i,j)= (telhs(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(telhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+(telhs(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(telhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-(telhs(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(telhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
                    Mothk0(i,j)= (telhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                    Mothk1(i,j)= ((telhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-((telhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                    Mothk2(i,j)= ((telhs(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+((telhs(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-((telhs(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       terhs(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
            case 9 
              if(j<=3 || j==9)
                  Mlamk0(i,j)= nlhs(1.0,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(1,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(nlhs(0.0,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                  Mlamk1(i,j)= (nlhs(1.0,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(1,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(nlhs(0.0,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-(nlhs(1.0,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(1,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(nlhs(0.0,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                  Mlamk2(i,j)= (nlhs(1.0,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(1,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(nlhs(0.0,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+(nlhs(1.0,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(1,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(nlhs(0.0,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-(nlhs(1.0,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(1,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)-(nlhs(0.0,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
                  Mothk0(i,j)= (nlhs(0.0,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2));
                  Mothk1(i,j)= ((nlhs(0.0,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))-((nlhs(0.0,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2;
                  Mothk2(i,j)= ((nlhs(0.0,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))+((nlhs(0.0,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,-1,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2))))/2-((nlhs(0.0,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)- ...
                       nrhs(0,0,invec2{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2)));
              else
                  Mlamk0(i,j)= 0.0;
                  Mlamk1(i,j)= 0.0;
                  Mlamk2(i,j)= 0.0;
                  Mothk0(i,j)= 0.0;
                  Mothk1(i,j)= 0.0;
                  Mothk2(i,j)= 0.0;
              end
            otherwise
                  ii=mod((i-9)-1,nq)+1;
                  jj=idivide(int32((i-9)-1),nq,'floor')+1;
                  Mlamk0(i,j)= feqlhs{ii,jj}(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)-(feqlhs{ii,jj}(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4));
                  Mlamk1(i,j)= (feqlhs{ii,jj}(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)-(feqlhs{ii,jj}(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4))-(feqlhs{ii,jj}(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)-(feqlhs{ii,jj}(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4))))/2;
                  Mlamk2(i,j)= (feqlhs{ii,jj}(1.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(1,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)-(feqlhs{ii,jj}(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4))+(feqlhs{ii,jj}(1.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(1,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)-(feqlhs{ii,jj}(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4))))/2-(feqlhs{ii,jj}(1.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(1,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)-(feqlhs{ii,jj}(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)));
                  Mothk0(i,j)= (feqlhs{ii,jj}(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4));
                  Mothk1(i,j)= ((feqlhs{ii,jj}(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4))-((feqlhs{ii,jj}(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4))))/2;
                  Mothk2(i,j)= ((feqlhs{ii,jj}(0.0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4))+((feqlhs{ii,jj}(0.0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,-1,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4))))/2-((feqlhs{ii,jj}(0.0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)- ...
                       feqrhs{ii,jj}(0,0,invec{j}{:},thv1,thn,tht1,tht2,thte1,thte2,thb1,thb2,thft,thfn,thfb,thfu,ft_simul,fu_simul,thf1,thf2,thf3,thfc1,thfc2,thf4)));
        end            
       end
    end
end
