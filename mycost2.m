function output=mycost2(krange,dt,t0,xin,nl,nsp,Mlamsimp_nocenter1,Mlamsimp1,Mothsimp_nocenter1,Mothsimp1)
  persistent Mlamsimp_nocenter
  persistent Mothsimp_nocenter
  persistent Mlamsimp
  persistent Mothsimp

  if(exist('Mlamsimp_nocenter1','var'))
  if(isempty(Mlamsimp_nocenter))
      Mlamsimp_nocenter=Mlamsimp_nocenter1;
      Mothsimp_nocenter=Mothsimp_nocenter1;
      Mlamsimp=Mlamsimp1;
      Mothsimp=Mothsimp1;
      return;
  end
  end
%   thv1=xin(1);
%   thn=xin(2);
%   tht1=xin(3);
%   tht2=xin(4);
%   thte1=xin(5);
%   thte2=xin(6);
%   thb1=xin(7);
%   thb2=xin(8);
%   thft=xin(9);
%   thfn=xin(10);
%   thfb=xin(11);
%   thfu=xin(12);
%   ft_simul=xin(13);
%   fu_simul=xin(14);
%   thf1=xin(15);
%   thf2=xin(16);
%   thf3=xin(17);
%   thfc1=xin(18);
%   thfc2=xin(19);
  xint=num2cell(xin);
  omegtmp=zeros(1,numel(krange));
  lambda=cell(1,numel(krange));
%   [Mlamk0,Mlamk1,Mlamk2,Mothk0,Mothk1,Mothk2]=M2f2(xint{:});
%   Mlam=@(k) Mlamk0+Mlamk1*k+Mlamk2*k^2;
%   Moth=@(k) Mothk0+Mothk1*k+Mothk2*k^2;
  parfor (i=1:numel(krange),8)
      Mlam=zeros(4+nl*nsp,4+nl*nsp);
      Moth=zeros(4+nl*nsp,4+nl*nsp);
      for k=0:20
        if (k==0)
            for j=1:3
               Mlam=Mlam+Mlamsimp_nocenter{j}*krange(i)^(j-1);
               Moth=Moth+Mothsimp_nocenter{j}*krange(i)^(j-1);
            end
        else
            for j=1:3
               Mlam=Mlam+Mlamsimp{k,j}*xint{k}*krange(i)^(j-1);
               Moth=Moth+Mothsimp{k,j}*xint{k}*krange(i)^(j-1);
            end
        end
      end
     %[Mlam,Moth]=M1f2(krange(i),xint{:});
     omegtmp(i)=max((imag(-log(eig(-inv(Mlam)*Moth))/1i/(dt/t0))));
     %lambda{i}=(-log(eig(-inv(Mlam)*Moth))/1i/(dt/t0));
     %omegtmp=max((imag(-log(eig(-inv(Mlam(krange(i)))*Moth(krange(i))))/1i/(dt/t0))));
%      if(omegtmp>omegmax)
%          omegmax=omegtmp;
%      end
  end
  output=max(omegtmp);
%   figure(5)
%   scatter(krange,real(cell2mat(lambda)));
%   figure(6)
%   scatter(krange,imag(cell2mat(lambda)));
end