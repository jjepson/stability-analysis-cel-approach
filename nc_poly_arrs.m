function [d0,d1,d2]=nc_poly_arrs(s,n,ar,br)
      m=n-1;
      d0=zeros(n,m+1);
      d1=zeros(n,m+1);
      d2=zeros(n,m+1);
    
      for k=1:n
        d0(k,0+1)=1.0;
        p1=d0(k,0+1);
        p2=0.0;
        for j=1:m
          p3=p2;
          p2=p1/br(j-1+1);
          p1=(s(k) - ar(j-1+1))*p2-br(j-1+1)*p3;
          d0(k,j+1) = p1;
        end
      end

 
        if (m<1) 
          d1(1:n,0+1) = 0.0;
        else
          for k=1:n
            d1(k,0+1) = 0.0;
            d1(k,1+1) = 1.0/br(0+1);
            p1=d1(k,1+1);
            p2=0.0;
            for j=2:m
              p3=p2;
              p2=p1/br(j-1+1);
              p1=(s(k) - ar(j-1+1))*p2-br(j-1+1)*p3+d0(k,j-1+1)/br(j-1+1);
              d1(k,j+1) = p1;
            end
          end
        end
     


        if (m<2) 
          d2(1:n,0+1:1+1) = 0.0;
        else
          for k=1:n
            d2(k,0+1:1+1) = 0.0;
            d2(k,2+1) = 2.0/(br(0+1)*br(1+1));
            p1=d2(k,2+1);
            p2=0.0;
            for j=3:m
              p3=p2;
              p2=p1/br(j-1+1);
              p1=(s(k) - ar(j-1+1))*p2-br(j-1+1)*p3+2.0*d1(k,j-1+1)/br(j-1+1);
              d2(k,j+1) = p1;
            end
          end
        end

      for k=1:n 
        d0(k,0+1:m+1) = d0(k,0+1:m+1)./br(0+1:m+1); 
      end
        for k=1:n 
          d1(k,0+1:m+1) = d1(k,0+1:m+1)./br(0+1:m+1); 
        end
        for k=1:n 
          d2(k,0+1:m+1) = d2(k,0+1:m+1)./br(0+1:m+1); 
        end
end
