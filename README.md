# Stability Analysis - CEL Approach

This is my matlab code for a von Neumann stability analysis I did of a Chapman-Enskog-like drift kinetic closure approach in the plasma fluid code NIMROD. See "An analysis and successful benchmarking of the Chapman-Enskog-like (CEL) continuum kinetic closure approach algorithm in NIMROD" in Computer Physics Communications (2024) for more details.   

stability4testbetter.m is the main driver file.  Figures 1 and 2 are the real and imaginary parts of omega for the coupled fluid-kinetic approach.  Figures 3 and 4 are the real and imaginary parts of omega for just the fluid equations in NIMROD (without the kinetics). max\_growth\_rate\_fluidkinetic and max\_growth\_rate\_fluid output the maximum value for the imaginary part of omega for the fluid-kinetic system and the fluid system respectively.   

Here I will outline some of the variables that can be changed in this file (where the units of all variables are SI except tempurature which is in eV (with Boltzmann's constant absorbed in)):

* nsp is the number of speed collocation points used for the kinetics
* nl is the polynomial degree of the basis functions in pitch angle in velocity space
* 2\*np+nt is the number of finite element cells in pitch angle in velocity space
* cquad is the number of quadrature points used to evaluate integrals required for the linearized Fokker-Plank collision operator
* gll specifices whether to use gll spacing for the nodes of the finite element polynomials in pitch angle in velocity space; false sets the spacing to uniform
* ang is the angle the background magnetic field is assumed to take relative to the k^hat vector  
* udk is u\_0 dot k^hat
* dt is the timestep
* m\_i is the ion mass, t is the background ion temperature, n is the background electron number density, zeff is the effective atomic number for the ions, b0 is the background magnetic field, kpare is the parallel diffusivity in the electron temperature equation (multiplied by the reference number density), te is the background elecron temperature, m\_e is the electron mass
* eta is the magnetic diffusivity multiplied by the permeability of free space 
* nu\_ii is the ion collision frequency
* c0 is the coefficient of the semi-implicit operator in the momentum equation
* doCm tells whether or not to include the fluid moments of the collision operator in the kinetic equation
* momadcenter1 and momadcenter2 tell how to center the fluid moment terms (not including fluid moments of the collision operator) in the kinetic equation
* cmcenter1, cmcenter2, and cmcenter3 tell how to center the fluid moments of the collision operator in the kinetic equation
* diffart2center tells how to center the diffusion of the fluid moments of f\_i1 in the kinetic equation
* diffart2 is the diffusivity for the diffusion of the fluid moments of f\_i1
* domomad tells whether to include the fluid moment terms in the kinetic equation (not including fluid moments of the collision operator)
* krange is the plotting range for normalized k
* do\_opt tells whether to try to optimize the centerings using fmincon
* x is a list of the centerings.  Here, I have more centerings that can be varied than in the paper, so I have conveniently identified the theta's from the paper in the comments after each x  
* For the fluid model not including the kinetics, one can also set a parallel diffusivity for the ion temperature equation (multiplied by a reference ion number density), which is kpari, and a diffusivity for an isotropic viscosity in the momentum equation, which is iso\_visc 

stability3testbetter.m is the same, but uses a legendre polynomial expansion in pitch angle in velocity space.  Here nl is the number of legendre polynomials used in the expansion.   
