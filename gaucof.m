function [a,b,x,w]=gaucof(n,a,b,amu0,x,w) 
      z=zeros(n,n); 

      for i=1:n 
        z(i,i)=1.0; 
      end
      [a,b,z]=tqli(a,b,n,z); 
      [a,z]=eigsrt(a,z,n);
      for i=1:n 
        x(i)=a(i); 
        w(i)=amu0*z(1,i)^2; 
      end 
end
                                                                        
function [d,e,z]=tqli(d,e,n,z) 
      for i=2:n 
        e(i-1)=e(i);
      end
      e(n)=0.0; 
      for l=1:n 
        iter=0;
        break2=true;
        while(break2)
        for m=l:n-1 %1
          dd=abs(d(m))+abs(d(m+1)); 
          if (abs(e(m))+dd==dd) %goto 2
              break
          end
        end
        if(abs(e(m))+dd~=dd) 
            m=n;
        end
        if (m~=l)  %2  
          if(iter==30) 
              disp('too many iterations in tqli')
          end
          iter=iter+1; 
          g=(d(l+1)-d(l))/(2.0*e(l)); 
          r=pythag(g,1.0); 
          g=d(m)-d(l)+e(l)/(g+f90sign(r,g)); 
          s=1.0;
          c=1.0;
          p=0.0;
          for i=m-1:-1:l 
            f=s*e(i); 
            b=c*e(i); 
            r=pythag(f,g); 
            e(i+1)=r; 
            break1=false;
            if (r==0.0) 
              d(i+1)=d(i+1)-p; 
              e(m)=0.0; 
              break1=true;
              break %goto 1 
            end
            s=f/r; 
            c=g/r;
            g=d(i+1)-p; 
            r=(d(i)-g)*s+2.0*c*b; 
            p=s*r; 
            d(i+1)=g+p; 
            g=c*r-b;
%     Omit lines from here ...                                          
            for k=1:n 
              f=z(k,i+1); 
              z(k,i+1)=s*z(k,i)+c*f; 
              z(k,i)=c*z(k,i)-s*f;
            end 
%     ... to here when finding only eigenvalues.                        
          end
          if(~break1)
            d(l)=d(l)-p; 
            e(l)=g; 
            e(m)=0.0; 
            %goto 1 
          end
        else
            break2=false;
        end
        end
      end
 end
                                                                        
 function [d,v]=eigsrt(d,v,n) 
      for i=1:n-1 
        k=i; 
        p=d(i); 
        for j=i+1:n 
          if (d(j)>=p) 
            k=j; 
            p=d(j); 
          end
        end
        if (k~=i)
          d(k)=d(i); 
          d(i)=p ;
          for j=1:n 
            p=v(j,i) ;
            v(j,i)=v(j,k); 
            v(j,k)=p ;
          end 
        end
      end
 end
                                                                        
 function output=pythag(a,b) 
      absa=abs(a); 
      absb=abs(b); 
      if(absa>absb) 
        output=absa*sqrt(1.0+(absb/absa)^2); 
      else 
        if(absb==0.0) 
          output=0.0; 
        else 
          output=absb*sqrt(1.0+(absa/absb)^2); 
        end 
      end 
 end