function [x,w]=lobleg(x1,x2,n)
   x=zeros(1,n); w=zeros(1,n);
   eps=1.e-15;
   xgau=zeros(1,n-1);
   wgau=zeros(1,n-1);
   nleg=n-1;
   [xgau,wgau]=gauleg(-1.0,1.0,nleg);

   m=(n+1)/2;
   xm=0.5*(x2+x1);
   xl=0.5*(x2-x1);
   fac=2*xl/(n*(n-1));

   w(1)=fac;
   x(1)=x1;
   w(n)=fac;
   x(n)=x2;
   for i=2:m
      z=0.5*(xgau(i)+xgau(i-1));
      while(true)
        p1=1;
        p2=0;
        for j=1:nleg
          p3=p2;
          p2=p1;
          p1=((2*j-1)*z*p2-(j-1)*p3)/(j);
        end
        gg=(nleg)*(z*p1-p2);
        dg=(nleg*(nleg+1))*p1;
        z1=z;
        z=z1-gg/dg;
        if (abs(z-z1)<=eps) 
            break
        end
      end
      w(i)=fac/p1^2;
      w(n+1-i)=w(i);
      x(i)=xm+xl*z;
      x(n+1-i)=xm-xl*z;
   end
end